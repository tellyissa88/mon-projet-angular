import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppareilService {
  appareilsSubject = new Subject<any[]>();
  constructor(private httpClient: HttpClient) { }
   private  appareils = [
        {
          id : 1,
          name: 'Machine à laver',
          status: 'eteint'
        },
        {
          id : 2,
          name: 'Frigo',
          status: 'allumer'
        },
        {
          id : 3,
          name: 'Ordinateur',
          status: 'eteint'
        }
      ];
      emitAppareilSubject() {
        this.appareilsSubject.next(this.appareils.slice());
      }
      switchOnAll() {
        for (let appareil of this.appareils) {
            appareil.status = 'allumer';
        }
        this.emitAppareilSubject();
    }
    switchOffAll(){
        for (let appareil of this.appareils) {
            appareil.status = 'eteint';
        }
        this.emitAppareilSubject();
    }
    switchOnOne(index : number) {
    this.appareils[index].status = 'allumer';
    this.emitAppareilSubject();
    }
    switchOnOff(index : number){
        this.appareils[index].status = 'eteint';
        this.emitAppareilSubject();
     }
     getAppareilById(id: number) {
      const appareil = this.appareils.find(
        (s) => {
          return s.id === id;
        }
      );
      return appareil;
  }
  addAppareil(name: string, status: string) {
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
    this.appareils.push(appareilObject);
    this.emitAppareilSubject();
}
saveAppareilsToServer() {
  this.httpClient
    .put('https://premierprojetangular-4afef.firebaseio.com/premierprojetangular.json', this.appareils)
    .subscribe(
      () => {
        console.log('Enregistrement terminé !');
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
}
getAppareilsFromServer() {
  this.httpClient
    .get<any[]>('https://premierprojetangular-4afef.firebaseio.com/premierprojetangular.json')
    .subscribe(
      (response) => {
        this.appareils = response;
        this.emitAppareilSubject();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
}
}
